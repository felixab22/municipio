import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: any = '';
  password: any = '';
  constructor(
    private _router: Router
  ) {

  }

  ngOnInit() {
  }
  login(user, pass) {
    console.log(user, pass);
    this._router.navigate(['/Muni/Inicio/Bienvenido'])
  }
}
