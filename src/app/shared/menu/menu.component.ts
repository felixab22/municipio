import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  public itemsList: Object[] = [
    {
      title: 'ADMINISTRACIÓN',
      boton1: 'Boton',
      boton2: 'Boton',
      boton3: 'Boton',
    },
    {
      title: 'REPORTES',
      boton1: 'LISTA',
      boton2: 'CONSOLIDADO',
      boton3: 'PROCEDIMIENTO',
    },
    {
      title: 'Menu3',
      boton1: 'Boton',
      boton2: 'Boton',
      boton3: 'Boton',
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}
