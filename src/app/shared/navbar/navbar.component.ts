import { Component, OnInit,Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Output() Cambiomenu: EventEmitter<any> = new EventEmitter();
  _opened: boolean = false;
  constructor() { }

  ngOnInit() {
  }
  _toggleSidebar() {
    this._opened = !this._opened;
    this.Cambiomenu.emit(this._opened);
  }
}
