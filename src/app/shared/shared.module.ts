import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { MDBSpinningPreloader, MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { AgmCoreModule } from '@agm/core';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { SidebarModule } from 'ng-sidebar';



@NgModule({
  declarations: [
    // LoginComponent, 
    MenuComponent, 
    NavbarComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SidebarModule.forRoot(),
    MDBBootstrapModulesPro.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: 'Your_api_key'
    })
  ],
  exports:[
    // LoginComponent,
    MenuComponent,
    NavbarComponent
  ],
  providers: [MDBSpinningPreloader],
  schemas:      [ NO_ERRORS_SCHEMA ]
})
export class SharedModule { }
