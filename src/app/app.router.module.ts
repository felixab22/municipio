/* cSpell:disable */
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages/pages.component';
import { LoginComponent } from './shared/login/login.component';


const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'Muni',
    component: PagesComponent,
    children:
      [
        {
          path: 'Inicio',
          loadChildren: './pages/general/general.module#GeneralModule'
        },
        {
          path: 'Mantenimiento',
          loadChildren: './pages/mantenimiento/mantenimiento.module#MantenimientoModule'
        },
      ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
