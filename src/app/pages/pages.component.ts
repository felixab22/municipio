import { Component } from '@angular/core';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent  {

  
  _opened = false;
  _closeOnClickOutside: boolean = true;
  _closeOnClickBackdrop: boolean = true;
  _animate: boolean = true;
  _dock: boolean = true;
  public push = 'push';
  
  constructor() { }

  cambiomenu(event: boolean) {
    console.log(event);
    this._opened = event;

  }
  cambio() {
    if (this._opened === false) {
      this._opened = true;
    }
  }
}
