import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { SidebarModule } from 'ng-sidebar';



@NgModule({
  declarations: [PagesComponent],
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
    SharedModule,
    SidebarModule.forRoot(),
  ]
})
export class PagesModule { }
