import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GeneralRoutingModule } from './general-routing.module';
import { BienvenidoComponent } from './bienvenido/bienvenido.component';


@NgModule({
  declarations: [BienvenidoComponent],
  imports: [
    CommonModule,
    GeneralRoutingModule
    
  ]
})
export class GeneralModule { }
